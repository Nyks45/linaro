/* Generated automatically. */
static const char configuration_arguments[] = "../gcc-linaro54/configure --build=x86_64-linux-gnu --target=x86_64-linux-gnu --with-gcc-version=Linaro540616 --with-sysroot=/ --with-isysroot=/mnt/d --with-tune=intel --with-arch=x86-64 --with-fpmath=sse --enable-lto --enable-multilib-list=m32,m64,mx32 --enable-plugins --enable-multiarch --prefix=/mnt/d/install --program-transform-name='s&^&x86_64-linux-&' --with-isl --with-cloog --with-ppl --disable-headers --enable-languages=c,c++ --enable-graphite=yes --enable-gold=default --disable-option-checking --disable-bootstrap --enable-threads --disable-nls --with-gnu-ld --with-gnu-as --enable-multiarch --with-host-libstdcxx='-static-libgcc -Wl,-Bstatic,-lstdc++,-Bdynamic -lm' --enable-eh-frame-hdr-for-static --enable-initfini-array --enable-cloog-backend=isl --disable-libssp --enable-shared --with-python";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" }, { "tune", "intel" } };
